# Generated by Django 3.1 on 2020-10-06 23:17

import datetime
import django.core.validators
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Student',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('name', models.CharField(max_length=255)),
                ('birth_date', models.DateField(default=datetime.date(2008, 1, 1))),
                ('total', models.FloatField(validators=[django.core.validators.MaxValueValidator(400)])),
                ('Expenses', models.CharField(choices=[('تم السداد', 'تم السداد'), ('لم يسدد', 'لم يسدد')], default='لم يسدد', max_length=20)),
                ('computed_age', models.CharField(max_length=255, null=True)),
                ('percentage', models.CharField(max_length=255, null=True)),
                ('created_at', models.DateTimeField(auto_now_add=True)),
            ],
            options={
                'ordering': ['-total'],
                'unique_together': {('total', 'birth_date')},
            },
        ),
    ]
