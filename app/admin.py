from django.contrib import admin
from django.contrib.auth.models import Group
from .models import Student
from import_export.admin import ImportExportModelAdmin


@admin.register(Student)
class BookResource(ImportExportModelAdmin):
    class Meta:
        model = Student


# admin.site.register(Student)
admin.site.unregister(Group)
