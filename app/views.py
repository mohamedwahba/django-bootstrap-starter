from django.shortcuts import render, get_object_or_404, redirect
from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.contrib.auth.decorators import login_required
from .forms import StudentForm
from django.contrib import messages
from django.http import HttpResponse
import datetime
import xlwt
from django.template.loader import render_to_string
from weasyprint import HTML
import tempfile

from .models import Student


def main(request):
    return render(request, 'main.html')


def statics(request):
    all_student = Student.objects.count()
    expense_student = Student.objects.filter(Expenses='تم السداد').count()
    no_expense_student = Student.objects.filter(Expenses='لم يسدد').count()
    return render(request, 'statics.html', {"all_student": all_student, "expense_student": expense_student,
                                            "no_expense_student": no_expense_student})


@login_required
def home(request):
    queryset = Student.objects.all()
    page = request.GET.get('page', 1)
    paginator = Paginator(queryset, 10)
    try:
        topics = paginator.page(page)
    except PageNotAnInteger:
        topics = paginator.page(1)
    except EmptyPage:
        topics = paginator.page(paginator.num_pages)

    return render(request, 'home.html', {'topics': topics})


@login_required
def add_student(request):
    if request.method == "POST":
        form = StudentForm(request.POST)
        if form.is_valid():
            form.save()

            messages.success(request, 'تم اضافه الطالب بنجاح')
    else:
        form = StudentForm()

    return render(request, 'add_student.html', {'form': form})


@login_required
def student_remove(request, student_id):
    student = get_object_or_404(Student, pk=student_id)
    student.delete()
    return redirect('home')


@login_required()
def student_edit(request, student_id):
    student = get_object_or_404(Student, pk=student_id)
    print(student.birth_date)
    form = StudentForm(request.POST or None, instance=student)
    if form.is_valid():
        form.save()
        return redirect('home')
    return render(request, 'edit_student.html', {'form': form})


def export_csv(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=students' + str(datetime.datetime.now()) + '.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Students')
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    columns = ['المصروفات', 'السن في 10/1', 'المجموع', 'تاريخ الميلاد', 'اسم الطالب']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    font_style = xlwt.XFStyle()
    rows = Student.objects.all().values_list('Expenses', 'computed_age', 'total', 'birth_date', 'name')

    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, str(row[col_num]), font_style)

    wb.save(response)
    return response


def export_pdf(request):
    response = HttpResponse(content_type='application/ms-excel')
    response['Content-Disposition'] = 'attachment; filename=students' + str(datetime.datetime.now()) + '.xls'
    wb = xlwt.Workbook(encoding='utf-8')
    ws = wb.add_sheet('Students')
    row_num = 0
    font_style = xlwt.XFStyle()
    font_style.font.bold = True
    columns = ['المصروفات', 'السن في 10/1', 'المجموع', 'تاريخ الميلاد', 'اسم الطالب']
    for col_num in range(len(columns)):
        ws.write(row_num, col_num, columns[col_num], font_style)
    font_style = xlwt.XFStyle()
    rows = Student.objects.filter(created_at__gt=datetime.datetime(2020, 10, 7, 2, 0 , 0, 0)).all().values_list('Expenses', 'computed_age', 'total', 'birth_date', 'name')

    for row in rows:
        row_num += 1
        for col_num in range(len(row)):
            ws.write(row_num, col_num, str(row[col_num]), font_style)

    wb.save(response)
    return response



# def export_pdf(request):
#     response = HttpResponse(content_type='application/pdf')
#     response['Content-Disposition'] = 'attachment; filename=students' + str(datetime.datetime.now()) + '.pdf'
#     response['Content-Transfer-Encoding'] = 'binary'
#     students = Student.objects.all()
#
#     html_string = render_to_string('pdf-html.html', {'students': students, 'total': 0})
#     html = HTML(string=html_string)
#     result = html.write_pdf()
#     with tempfile.NamedTemporaryFile(delete=True) as output:
#         output.write(result)
#         output.flush()
#
#         output = open(output.name, 'rb')
#
#         response.write(output.read())
#
#     return response
